<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentLike extends Model
{
    //
    protected $fillable =[
        'sub_comment_id' ,
        'user_id',
        'like',
    ];



    public function user()
    {
        $this->belongsTo(User::class);
    }

    public function subComment()
    {
        $this->belongsTo(SubComment::class);
    }

    public static function likeTableCheck($user_id,$sub_id)
    {

        if($like = static::where('user_id',$user_id)->where('sub_comment_id',$sub_id)->get()->first()) {
            return $like->like;
        }
        else{
            return 3;
        }

    }

}
