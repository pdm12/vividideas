<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    //
    protected $fillable =[
        'submission_id' ,
        'user_id',
        'badge',
    ];



    public function user()
    {
        $this->belongsTo(User::class);
    }

    public function submission()
    {
        $this->belongsTo(Submission::class);
    }

    public static function badgeTableCheck($user_id,$sub_id)
    {

        if($badge = static::where('user_id',$user_id)->where('submission_id',$sub_id)->get()->first()) {
            return $badge->badge;
        }
        else{
            return 10;
        }

    }

}
