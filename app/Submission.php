<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    //
    protected $fillable =[
        'submission_topic',
        'submission' ,
        'user_id',
        'category_id',
        'video_path',
    ];

    public function path()
    {
        return '/submissions/' . $this->id;
    }

    public function pathEdit()
    {
        return '/admin/submissions/' . $this->id . '/edit';
    }

    /**
     * A submission has many photos
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(SubComment::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function badges()
    {
        return $this->hasMany(Badge::class);
    }

    /**
     * A submission post may contain many photos
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     */
    public function photos()
    {
        return $this->hasMany(SubmissionPhoto::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    /**
     * The Submission is owned by a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\User','user_id');
    }

    /**
     * To get the submission which has the referenced id
     * @param $id
     * @return mixed
     */
    public static function submissionLocatedAt($id)
    {
        return static::where(compact('id'))->firstOrFail();
    }

    public static function getLastSubmission()
    {
        return static::all()->sortByDesc('id')->first();
    }

    public static function subPoints($id){
        //$comments = static::where('user_id',$id)->firstOrFail()->comments->count();
        return $submissions = static::where('user_id',$id)->get()->count();
        //return ($comments + $submissions);
    }

    public static function commentPoints($id){
        return static::where('user_id',$id)->firstOrFail()->comments->count();
    }

    public static function subUpVotes($id)
    {
        $allLikes = static::where('user_id',$id)->firstOrFail()->likes;
        $upCount = 0 ;

        foreach ($allLikes as $upVote){
            if($upVote->like == 1){
                $upCount=$upCount+1;
            }
        }
        return $upCount;

    }

    public static function subDownVotes($id)
    {
        $allLikes = static::where('user_id',$id)->firstOrFail()->likes;
        $downCount = 0 ;

        foreach ($allLikes as $downVote){
            if($downVote->like == 0){
                $downCount=$downCount+1;
            }
        }
        return $downCount;

    }

    public static function badgeGreat($id)
    {
        $allbadges = static::where('user_id',$id)->firstOrFail()->badges;
        $badgeCount = 0 ;

        foreach ($allbadges as $badgeVote){
            if($badgeVote->badge == 1){
                $badgeCount=$badgeCount+1;
            }
        }
        return $badgeCount;
    }

    public static function badgeWelldone($id)
    {
        $allbadges = static::where('user_id',$id)->firstOrFail()->badges;
        $badgeCount = 0 ;

        foreach ($allbadges as $badgeVote){
            if($badgeVote->badge == 2){
                $badgeCount=$badgeCount+1;
            }
        }
        return $badgeCount;
    }

    public static function badgeExcellent($id)
    {
        $allbadges = static::where('user_id',$id)->firstOrFail()->badges;
        $badgeCount = 0 ;

        foreach ($allbadges as $badgeVote){
            if($badgeVote->badge == 3){
                $badgeCount=$badgeCount+1;
            }
        }
        return $badgeCount;
    }

    public static function badgeAwesome($id)
    {
        $allbadges = static::where('user_id',$id)->firstOrFail()->badges;
        $badgeCount = 0 ;

        foreach ($allbadges as $badgeVote){
            if($badgeVote->badge == 4){
                $badgeCount=$badgeCount+1;
            }
        }
        return $badgeCount;
    }




        /**
     * To add photo to its database which belongs to the submission
     * @param SubmissionPhoto $photo
     * @return Model
     */
    public function addSubmissionPhoto(SubmissionPhoto $photo)
    {
        return $this->photos()->save($photo); //TO add photo belonging to the submission
    }






//    public function getPriceWithoutRs($price)
//    {
//
//       return $price = str_replace('','Rs ', $price);
//
//    }



    /**
     * To check if the given user created the submission
     * @param User $user
     * @return bool
     */
    public function ownedBy(User $user)
    {
        return $this->user_id == $user->id;
    }

    public function getVideoPathAttribute($video_path)
    {
        return str_replace('watch?v=','embed/', $video_path);
    }




}
