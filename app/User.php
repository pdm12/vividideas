<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function owns($relation)
    {
        return $relation->user_id == $this->id;
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function getRoleName()
    {
        $role = Role::all()->where('id', $this->role_id)->first();

        $roleName = $role->name;

        return $roleName;
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }


    public function pathEdit()
    {
        return '/admin/profile/' . $this->id . '/edit';
    }
}
