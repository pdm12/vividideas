<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class SubComment extends Model
{
    //
    protected $fillable =[
        'comment',
        'submission_id' ,
        'user_id',
        'video_path',
        'vote_count',
    ];

    /**
     * The Category belongs to a submission
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSubmission()
    {
        return $this->belongsTo(Submission::class,'submission_id');
    }

    public function commentOwner()
    {
        return $this->belongsTo('App\User','user_id');
    }


    public function getVideoPathAttribute($video_path)
    {
             return str_replace('watch?v=','embed/', $video_path);
    }

    public function commentLikes()
    {
        return $this->hasMany(CommentLike::class);
    }

    public static function commentUpVotes($id)
    {
        $allLikes = static::where('user_id',$id)->firstOrFail()->commentLikes;
        
        $upCount = 0 ;

        foreach ($allLikes as $upVote){
            if($upVote->like == 1){
                $upCount=$upCount+1;
            }
        }
        return $upCount;

    }

    public static function commentDownVotes($id)
    {
        $allLikes = static::where('user_id',$id)->firstOrFail()->commentLikes;
        $downCount = 0 ;

        foreach ($allLikes as $downVote){
            if($downVote->like == 0){
                $downCount=$downCount+1;
            }
        }
        return $downCount;

    }


    
    public function pathEdit()
    {
        return '/admin/comments/' . $this->id . '/edit';
    }
}
