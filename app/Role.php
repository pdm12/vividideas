<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name', 'description'];

    public function pathEdit()
    {
        return '/admin/roles/' . $this->id . '/edit';
    }

   
}
