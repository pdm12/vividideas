<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['name', 'description', 'parent_id'];

    public function pathEdit()
    {
        return '/admin/categories/' . $this->id . '/edit';
    }

    public function submission()
    {
        return $this->hasMany(Submission::class);

    }

    public function pathView()
    {
        return '/category/' . $this->id;
    }


    public function getCategories()
    {
        $categories = Category::all()->where('parent_id', 0);
        $categories = $this->addRelation($categories);

        return $categories;
    }

    public function selectChild($id)
    {
        $categories = Category::all()->where('parent_id', $id);

        $categories = $this->addRelation($categories);
        return $categories;
    }

    public function addRelation($categories)
    {
        $categories->map(function($item, $key)
        {
            $sub = $this->selectChild($item->id);
            return $item = array_add($item, 'subCategory', $sub);
        });

        return $categories;
    }

    public function getParent()
    {
        $parent = $this->parent_id;
        $parentName = Category::all()->where('id', $parent)->first();

        if(!$parentName)
        {
            $cat = 'None';
        }
        else
        {
            $cat = $parentName->name;
        }

        return $cat;
    }

    public function checkChildExist()
    {
        $check = '';
        if($this->parent_id == 0)
        {
            $subCategory = Category::all()->where('parent_id', $this->id)->first();
            if($subCategory)
            {
                $check = 'disabled';
                $message = 'You';
            }
        }

        return $check;
    }

}
