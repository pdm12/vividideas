<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubmissionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * true if user has authorization and viceversa for false
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'submission_topic'  => 'required',
            'submission' => 'required',
            'category_id' => 'required',


        ];
    }
}
