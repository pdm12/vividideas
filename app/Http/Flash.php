<?php

namespace App\Http;

class Flash{

    /**
     * @param $title
     * @param $message
     * @param $level
     * @param string $key
     * @return mixed
     */
    public function create($title, $message,$level,$key='flash_message')
    {
        return session()->flash($key,[
            'title' => $title,
            'message' => $message,
            'level' => $level
        ]);
    }

    /**
     * @param $title
     * @param $message
     * @return mixed
     */
    public function info($title, $message)
    {
        return $this->create($title, $message,'info');

    }

    /**
     * @param $title
     * @param $message
     * @return mixed
     */
    public function success($title, $message)
    {
        return $this->create($title, $message,'success');

    }

    /**
     * @param $title
     * @param $message
     * @return mixed
     */
    public function error($title, $message)
    {
        return $this->create($title, $message,'error');

    }

    public function warning($title, $message)
    {
        return $this->create($title, $message,'warning');

    }

    /**
     *
     * $level is success by default , it can be overrriden if needed
     *
     * @param $title
     * @param $message
     * @param string $level
     * @return mixed
     */
    public function overlay($title, $message ,$level='success')
    {
        return $this->create($title, $message,$level,'flash_message_overlay');

    }


    //Overlay : read and click a button

}

//$flash->message('Hello There');
//$flash->error('Hello There');
//$flash->aside('Error!','Hello There');
//$flash->success('Success!','You have successfully..');
//flash()->overlay('Welcome Aboard','Thankyou for signing up!');
