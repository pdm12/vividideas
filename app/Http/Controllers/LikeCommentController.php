<?php

namespace App\Http\Controllers;

use App\CommentLike;
use App\SubComment;
use Illuminate\Http\Request;

use App\Http\Requests;

class LikeCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        //
        $likeCheck = CommentLike::likeTableCheck($request['user_id'],$request['sub_comment_id']);

        $comment = SubComment::find($id);

        if($likeCheck== 1 || $likeCheck== 0  )//If its already liked or disliked
        {
                $vote_count = $comment->vote_count;
            if($request['like']==1) //Like
            {
                if( CommentLike::where('user_id', $request->user_id)->where('sub_comment_id', $request->sub_comment_id)->update([
                    'like' => 1,
                ]))
                {
                    if($likeCheck== 1){
                    return redirect()->back();
                    }
                    else
                    {
                        $vote_count = $vote_count+1;
                        if (SubComment::where('id', $id)->update(['vote_count' => $vote_count])) {
                            //   flash()->success('Success!', 'Liked');
                        }
                    }
                }
                else{
                    flash()->error('Oops!', 'Please check whether the fields are properly entered!');
                    return redirect()->back();
                }
            } //IF LIKE == 1 END

            if($request['like']==0) //Dislike
            {
                $vote_count = $vote_count-1;

                if( CommentLike::where('user_id', $request->user_id)->where('sub_comment_id', $request->sub_comment_id)->update([
                    'like' => 0,

                ]))
                {
                    if($likeCheck== 0){
                        return redirect()->back();
                    }
                    if(SubComment::where('id',$id)->update(['vote_count'=>$vote_count]))
                    {
                     //   flash()->success('Success!', 'DisLiked');
                    }
                }
                else{
                    flash()->error('Oops!', 'Please check whether the fields are properly entered!');
                    return redirect()->back();
                }
            }

        }else// IF its a new like
        {
            if( CommentLike::create($request->all()) ) {
                //Flash Message
                $vote_count = $comment->vote_count;
                $vote_count = $vote_count+1;
                if (SubComment::where('id', $id)->update(['vote_count' => $vote_count]))
                {
                    flash()->success('Success!', 'Liked!');
                }
            }
            else{

                flash()->error('Oops!', 'Please check whether the fields are properly entered!');

                return redirect()->back();
            }

        }//End Of Last Else

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
