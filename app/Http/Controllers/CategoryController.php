<?php

namespace App\Http\Controllers;

use App\Category;

use Illuminate\Http\Request;

use App\Http\Requests;



class CategoryController extends Controller
{
    public function index()
    {
        /*$categories = Category::all()->where('parent_id', 0);*/
        $category = new Category();
        $categories = $category->getCategories();
        return view('admin.categories', compact('categories'));
    }

    public function edit(Category $category)
    {
        //$allCategories = $category->getCategories();
        $categories = Category::where('id', '!=', 15)->where('parent_id', 0)->get();



       // return view('admin.category-edit', compact('category'));
        return view('admin.category-edit', ['category'=>$category,
            'categories'=>$categories
        ]);
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories,name'
        ]);

        $category = new Category;

        $category->name = $request->name;
        $category->description = $request->catDescription;
        $category->parent_id = $request->catParent;
        $category->save();

        flash()->success('Success', 'Category has been added successfully!');

        return back();
    }

    public function delete($id)
    {

        $category = Category::find($id);

        if($category->parent_id == 0)
        {
            $subCategory = Category::all()->where('parent_id', $category->id)->first();
            if($subCategory)
            {
                flash()->warning('Warning', 'This category contains sub categories. Please delete the sub categories first!');
            } else
            {
                $category->delete();
                flash()->success('Success', 'Category has been deleted successfully!');
            }

        } else
        {
            $category->delete();
            flash()->success('Success', 'Category has been deleted successfully!');
        }

        return back();
    }

    public function update(Request $request, Category $category)
    {

        $this->validate($request, [
            'name' => 'required'
        ]);


        $name = $category->name;
        $requestedName = $request->name;

        $categories = Category::all()->where('name', $requestedName)->first();
        if (($categories) && ($categories->name != $name)){
            flash()->warning('Warning', 'Category name already exists!');
        }
        else {
            $category->update($request->all());
            flash()->success('Success', 'Category has been updated successfully!');
        }


        return back();
    }
    
    
    

}
