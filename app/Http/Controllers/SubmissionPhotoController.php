<?php

namespace App\Http\Controllers;

use App\SubmissionPhoto;
use Illuminate\Http\Request;

use App\Http\Requests;

class SubmissionPhotoController extends Controller
{
    //
    public function destroy($id)
    {
        
        SubmissionPhoto::findOrFail($id)->delete();

        return back();

    }
}
