<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Role;

class adminUsersController extends Controller
{
    

    public function index()
    {
        $users = User::all();

        return view('admin.users', compact('users'));
    }

    public function userNewIndex()
    {
        $roles = Role::all();

        return view('admin.user-new', compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            
            'name' => 'required|max:8|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);

        $user = new User;


        $user->name = $request->name;
        $user->email = $request->email;
        $password = $request->password;

        $user->password = bcrypt($password);
        $user->role_id = $request->role_id;

        $user->save();

        flash()->success('Success', 'User has been successfully created!');

        return back();
    }

    public function edit(User $user)
    {
        return view('admin.profile', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|max:255'
        ]);

        $users = User::all()->where('email', $request->email)->first();
        if(($users) && ($users->email != $user->email))
        {
            flash()->overlay('Warning', 'Email: ' . $request->email . ' is already being used by another user!', 'warning');
        }
        else
        {
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->bio = $request->bio;
            $user->email = $request->email;
            $user->update();

            flash()->success('Success', 'User has been successfully updated!');
        }

        return back();
    }

    public function status($id)
    {
        $user = User::find($id);

        $status = $user->status;

        if($status == 1)
        {
            $user->status = 2;
        }
        else
        {
            $user->status = 1;
        }

        $user->update();

        return back();
    }
}
