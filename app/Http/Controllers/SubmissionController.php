<?php

namespace App\Http\Controllers;

use App\CommentLike;
use App\Like;
use App\SubComment;
use App\Submission;
use App\SubmissionPhoto;
use Illuminate\Http\Request;

use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Auth;

use App\Http\Requests;
use App\Http\Requests\SubmissionRequest;

use App\Category;
use App\Http\Controllers\Traits\AuthorizesUsers;



class SubmissionController extends Controller
{
    use AuthorizesUsers;

    public function __construct()
    {

        $this->middleware('auth'); //Gets the auth middleware

        parent::__construct(); //TO delegate up and check parent controller
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $subPoints = Submission::subPoints(\Auth::user()->id);//Calculates user points and returns
        $commentPoints = Submission::commentPoints(\Auth::user()->id);//Calculates user points and returns

        $upSubVotes = Submission::subUpVotes(\Auth::user()->id);//Calculates user points and returns
        $downSubVotes = Submission::subDownVotes(\Auth::user()->id);//Calculates user points and returns

        $upCommentVotes = SubComment::commentUpVotes(\Auth::user()->id);//Calculates user points and returns
        $downCommentVotes = SubComment::commentDownVotes(\Auth::user()->id);//Calculates user points and returns

        $totalUpVotes = $upSubVotes + $upCommentVotes;
        $totalDownVotes = $downSubVotes + $downCommentVotes;

        $votesDiff = $totalUpVotes - $totalDownVotes;

        $GreatVotes = Submission::badgeGreat(\Auth::user()->id);
        $welldoneVotes = Submission::badgeWelldone(\Auth::user()->id);
        $excellentVotes = Submission::badgeExcellent(\Auth::user()->id);
        $awesomeVotes = Submission::badgeAwesome(\Auth::user()->id);


        $submission = Submission::where('user_id',\Auth::user()->id )->get();

        return view('submissions.index', ['submissions'=>$submission,
            'subPoints'=>$subPoints,
            'commentPoints'=>$commentPoints,
            'totalUpVotes'=>$totalUpVotes,
            'totalDownVotes'=>$totalDownVotes,
            'votesDiff'=>$votesDiff,
            'greatVotes'=>$GreatVotes,
            'welldoneVotes'=>$welldoneVotes,
            'excellentVotes'=>$excellentVotes,
            'awesomeVotes'=>$awesomeVotes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $lastSub=Submission::getLastSubmission();

        $category = Category::all();
        return view('submissions.create', compact('category','lastSub'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,SubmissionRequest $submissionRequest)
    {
        //
        if( Submission::create($request->all()) ) {

            //Flash Message
            flash()->success('Success!', 'Submission is successfully posted');
        }else{

            flash()->error('Oops!', 'Please check whether the fields are properly entered!');

            return redirect()->back();

        }

        $lastSub=Submission::getLastSubmission();

      //  return redirect('/submissions/create');
        return redirect('/submissions/'.$lastSub->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $category = new Category();
        $categories = $category->getCategories();

        $categories = Category::all();
        
        $ifLike = Like::likeTableCheck(\Auth::user()->id ,$id );
        //$subTotalLikes = Submission::subTotalLikes(\Auth::user()->id);//Calculates user points and returns

       // $ifCommentLike = CommentLike::likeTableCheck(\Auth::user()->id,)
        
        $sub=Submission::submissionLocatedAt($id);

        return view('submissions.show', compact('sub','categories','ifLike'));
    }

    /* ADDED BY SHAN
    ---------------------------------- */
    public function adminIndex()
    {
        $submissions = Submission::all();

        return view('admin.submissions', compact('submissions'));
    }

    public function adminEdit(Submission $submission)
    {
        $category = Category::all();
        return view('admin.submission-edit',compact('category','submission'));
    }
    
    public function adminUpdate(Request $request, Submission $submission)
    {
        $this->validate($request, [
            'submission_topic' => 'required',
            'submission' => 'required'
        ]);

        $submission->update($request->all());

        return back();
    }

    public function adminDelete($id)
    {
        $submission = Submission::find($id);
        $submission->delete();

        return back();
    }
    
    /* END OF ADDED BY SHAN
    --------------------------------- */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = Category::all();

        $sub = Submission::submissionLocatedAt($id);

        return view('submissions.edit', compact('sub','category'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,SubmissionRequest $submissionRequest)
    {
        //
        //
        // Input the submission to database
        if( Submission::where('id', $id)->update([
            'category_id' => $request->category_id,
            'submission_topic' => $request->submission_topic,
            'submission' => $request->submission,
            'video_path' => $request->video_path
        ])){

            //Flash Message
            flash()->success('Success!', 'Submission is successfully updated');
        }else{

            flash()->error('Oops!', 'Please check whether the fields are properly entered!');

            return redirect()->back();

        }

        //Redirect to landing page
        // return redirect()->back();
        return redirect('/submissions');





    }

    /**
     *
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Submission::findOrFail($id)->delete();

        flash()->success('Deleted', 'The Submitted idea is deleted');
        return back();
    }

    /**
     * @param $id
     * @param Request $request
     * @return string
     *
     * validates the photo
     * makes the photoInstance by calling the method in Model Photo
     *
     */
    public function addPhoto($id,Request $request)
    {
        $this->validate($request,
            [
                'photo' => 'required|mimes:jpg,jpeg,png,bmp'

            ]);

        if(! $this->userCreatedSubmission($request)) { //If user didnt create submission

            return $this->unauthorized($request);
        }
        $photo = $this->makePhoto($request->file('photo'));

        Submission::submissionLocatedAt($id)->addSubmissionPhoto($photo); //Add photo which belongs to submission

        return 'Done';
    }



    /**
     * TO make photo instance
     * @param UploadedFile $file
     * @return $this
     */
    protected function makePhoto(UploadedFile $file)
    {
        return SubmissionPhoto::named($file->getClientOriginalName())
            ->move($file);
    }


//    public function postLikePost(Request $request)
//    {
//        $post_id = $request['postId'];
//        $is_like = $request['isLike'] === 'true';
//        $update = false;
//        $post = Post::find($post_id);
//        if (!$post) {
//            return null;
//        }
//        // $user = Auth::user();
//        // $like = $user->likes()->where('post_id', $post_id)->first();
//        $postt = new Submission();
//        $like = $postt->likes()->where('submission_id', $post_id)->first();
//
//        if ($like) {
//            $already_like = $like->like;
//            $update = true;
//            if ($already_like == $is_like) {
//                $like->delete();
//                return null;
//            }
//        } else {
//            $like = new Like();
//        }
//        $like->like = $is_like;
//        $like->user_id = 1;//$user->id;
//        $like->post_id = $post->id;
//        if ($update) {
//            $like->update();
//        } else {
//            $like->save();
//        }
//        return null;
//    }



}
