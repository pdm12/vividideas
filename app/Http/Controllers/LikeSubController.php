<?php

namespace App\Http\Controllers;
use App\Like;
use Illuminate\Http\Request;

use App\Http\Requests;

class LikeSubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //


    }

    /**
     * Store a newly created resource in storage.
     * submissions/{submissions}/like
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        //
        $likeCheck = Like::likeTableCheck($request['user_id'],$request['submission_id']);
        
        if($likeCheck== 1 || $likeCheck== 0  )//If its already liked or disliked
        {

            if($request['like']==1) //Like
            {
                if( Like::where('user_id', $request->user_id)->where('submission_id', $request->submission_id)->update([
                    'like' => 1,
                ])){
                    flash()->success('Success!', 'Liked');
                }
                else{
                    flash()->error('Oops!', 'Please check whether the fields are properly entered!');
                    return redirect()->back();
                }
            } //IF LIKE == 1 END

            if($request['like']==0) //Dislike
            {
                if( Like::where('user_id', $request->user_id)->where('submission_id', $request->submission_id)->update([
                    'like' => 0,
                ])){
                    flash()->success('Success!', 'Liked');
                }
                else{
                    flash()->error('Oops!', 'Please check whether the fields are properly entered!');
                    return redirect()->back();
                }
            }

        }else// IF its a new like
        {
            if( Like::create($request->all()) ) {
            //Flash Message
            flash()->success('Success!', 'Task is successfully posted');
        }
            else{

            flash()->error('Oops!', 'Please check whether the fields are properly entered!');

            return redirect()->back();
                }
            
        }//End Of Last Else

        return redirect()->back();
        
        //Main Like Check
//        if( Like::create($request->all()) ) {
//
//            //Flash Message
//            flash()->success('Success!', 'Task is successfully posted');
//        }else{
//
//            flash()->error('Oops!', 'Please check whether the fields are properly entered!');
//
//            return redirect()->back();
//        }
//        //Redirect to landing page
//         return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
