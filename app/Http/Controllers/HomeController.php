<?php

namespace App\Http\Controllers;

use App\Category;
use App\Submission;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct(); //TO delegate up and check parent controller

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = new Category();
        $categories = $category->getCategories();

        $subAll = Submission::all();

        return view('home', compact('categories','subAll'));
    }

    public function showCats($id){
        $cat = Category::find($id);
        $sub = Submission::where('category_id',$cat->id )->get();

        $category = new Category();
        $categories = $category->getCategories();

        return view('home', ['categories'=>$categories,'subAll'=>$sub]);
    }



}
