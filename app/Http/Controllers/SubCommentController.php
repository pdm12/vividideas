<?php

namespace App\Http\Controllers;

use App\SubComment;
use Illuminate\Http\Request;

use App\Http\Requests;
//TODO: Comment's ,Solve Comment Likes
class SubCommentController extends Controller
{
    /**
     * submissions/{submissions}/comment
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * submissions/{submissions}/comment/create
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * submissions/{submissions}/comment
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        //
        $this->validate($request, [
            'comment' => 'required',
            'submission_id' => 'required',
            'user_id' => 'required',
        ]);

        if( SubComment::create($request->all()) ) {

            //Flash Message
        flash()->success('Success!', 'Comment is successfully posted');
         }else{

        flash()->error('Oops!', 'Please check whether the fields are properly entered!');

        return redirect()->back();

    }
        return redirect()->back();

    }

    /**
     * submissions/{submissions}/comment/{comment}
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * submissions/{submissions}/comment/{comment}/edit
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * submissions/{submissions}/comment/{comment}
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($subId,$commentId,Request $request)
    {
        //
        // Input the task to database
        if( SubComment::where('id', $commentId)->update([
            'comment' => $request->comment,
            'video_path' => $request->video_path
        ])){

            //Flash Message
            flash()->success('Success!', 'Comment is successfully updated');
        }else{

            flash()->error('Oops!', 'Please check whether the fields are properly entered!');

            return redirect()->back();

        }

        return back();
    }

    /**
     * submissions/{submissions}/comment/{comment}
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($subId,$id)
    {
        //
        $comment = SubComment::find($id);

            $comment->delete();
            flash()->success('flash_message', 'Comment has been successfully deleted!');

        return back();

    }

    /* Admin
    -------------------------------------------- */
    public function adminIndex()
    {
        $comments = SubComment::all();

        return view('admin.comments', compact('comments'));
    }

    public function adminStatus($id)
    {
        $comment = SubComment::find($id);

        $status = $comment->comment_status;

        if($status == 1)
        {
            $comment->comment_status = 2;
        }
        else
        {
            $comment->comment_status = 1;
        }

        $comment->update();

        return back();
    }

    public function adminDelete($id)
    {
        $comment = SubComment::find($id);
        $comment->delete();

        return back();
    }
}
