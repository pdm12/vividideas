<?php

namespace App\Http\Controllers;

use App\Role;

use Illuminate\Http\Request;

use App\Http\Requests;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Role::all();

        return view('admin.roles', compact('roles'));
    }

    public function edit(Role $role)
    {

        return view('admin.roles-edit', compact('role'));
    }

    public function update(Request $request, Role $role)
    {

        $this->validate($request, [
            'name' => 'required'
        ]);

        $name = $role->name;
        $requestedName = $request->name;

        $roles = Role::all()->where('name', $requestedName)->first();
        if (($roles) && ($roles->name != $name)){
            flash()->warning('Warning', 'User role already exists!');
        }
        else {
            $role->update($request->all());
            flash()->success('Success', 'User role has been successfully updated!');
        }

        return back();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name'
        ]);

        $role = new Role;

        $role->name = $request->name;
        $role->description = $request->description;

        $role->save();

        flash()->success('Success', 'User role been added successfully!');

        return back();
    }

    public function delete($id)
    {

        $role = Role::find($id);
        $role->delete();

        flash()->success('Success', 'Role has been successfully deleted!');

        return back();
    }
    
    
}
