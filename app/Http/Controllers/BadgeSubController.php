<?php

namespace App\Http\Controllers;

use App\Badge;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Submission;

class BadgeSubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * submissions/{submissions}/badge
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        //
        $badgeCheck = Badge::badgeTableCheck($request['user_id'],$request['submission_id']);

        if($badgeCheck== 1 || $badgeCheck== 2 || $badgeCheck== 3 || $badgeCheck== 4  )//If its already liked or disliked
        {

            if($request['badge']==1) //Like
            {
                if( Badge::where('user_id', $request->user_id)->where('submission_id', $request->submission_id)->update([
                    'badge' => 1,
                ])){
                    flash()->success('Success!', 'You have given a badge');
                }
                else{
                    flash()->error('Oops!', 'Please check whether the fields are properly entered!');
                    return redirect()->back();
                }
            } //IF LIKE == 1 END

            if($request['badge']==2) //Dislike
            {
                if( Badge::where('user_id', $request->user_id)->where('submission_id', $request->submission_id)->update([
                    'badge' => 2,
                ])){
                    flash()->success('Success!', 'You have given a badge');
                }
                else{
                    flash()->error('Oops!', 'Please check whether the fields are properly entered!');
                    return redirect()->back();
                }
            }

            if($request['badge']==3) //Like
            {
                if( Badge::where('user_id', $request->user_id)->where('submission_id', $request->submission_id)->update([
                    'badge' => 3,
                ])){
                    flash()->success('Success!', 'You have given a badge');
                }
                else{
                    flash()->error('Oops!', 'Please check whether the fields are properly entered!');
                    return redirect()->back();
                }
            } //IF badge == 3 END

            if($request['badge']==4) //Like
            {
                if( Badge::where('user_id', $request->user_id)->where('submission_id', $request->submission_id)->update([
                    'badge' => 4,
                ])){
                    flash()->success('Success!', 'You have given a Awesome badge');
                }
                else{
                    flash()->error('Oops!', 'Please check whether the fields are properly entered!');
                    return redirect()->back();
                }
            } //IF badge == 4 END

        }else// IF its a new like
        {
            if( Badge::create($request->all()) ) {
                //Flash Message
                flash()->success('Success!', 'Badge is successfully given');
            }
            else{

                flash()->error('Oops!', 'Please check whether the fields are properly entered!');

                return redirect()->back();
            }

        }//End Of Last Else

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
