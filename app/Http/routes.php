<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/category/{id}', 'HomeController@index');


/*-----Submissions-----*/

Route::resource('submissions','SubmissionController');

Route::resource('submissions.comment','SubCommentController');

Route::resource('submissions.like','LikeSubController');

Route::resource('submissions.badge','BadgeSubController');



Route::post('submissions/{id}/badge',[
    // This is a named route. use route('store_like_sub')  to call this route
    'uses'=>'BadgeSubController@store',
    'as' => 'store_badge_sub'
]);



//Route::resource('submissions.comments.like','LikeCommentController');

Route::post('comments/{id}/like',[
    // This is a named route. use route('store_like_sub')  to call this route
    'uses'=>'LikeCommentController@store',
    'as' => 'store_like_comment'
]);

Route::post('submissions/{id}/like',[
    // This is a named route. use route('store_like_sub')  to call this route
    'uses'=>'LikeSubController@store',
    'as' => 'store_like_sub'
]);



Route::post('submissions/{id}/photos',[
    // This is a named route. use route('store_task_photo')  to call this route
    'uses'=>'SubmissionController@addPhoto',
    'as' => 'store_submission_photo'
]);
Route::delete('submissionphotos/{id}','SubmissionPhotoController@destroy');

Route::post('submissions/{id}/comment',[
    // This is a named route. use route('comment.create')  to call this route
    'uses'=>'SubCommentController@store',
    'as' => 'comment.create'
]);

Route::put('submissions/{subId}/comment/{commentId}',[
    // This is a named route. use route('comment.edit')  to call this route
    'uses'=>'SubCommentController@update',
    'as' => 'comment.edit'
]);


/*-----Admin-----*/
Route::group(['middleware' => 'admin'], function () {
    Route::get('/admin', function () {
        return view('admin.dashboard');
    });
    
    /*-----Categories-----*/
    Route::get('/admin/categories', 'CategoryController@index');
    Route::get('/admin/categories/{category}/edit', 'CategoryController@edit');
    Route::get('admin/categories/{category}/delete', 'CategoryController@delete');
    Route::post('/admin/categories/new', 'CategoryController@store');
    Route::patch('admin/categories/{category}', 'CategoryController@update');

    /*-----Submissions Admin-----*/
    Route::get('/admin/submissions', 'SubmissionController@adminIndex');
    Route::get('/admin/submissions/{submission}/edit', 'SubmissionController@adminEdit');
    Route::patch('admin/submissions/{submission}', 'SubmissionController@adminUpdate');
    Route::get('admin/submissions/{submission}/delete', 'SubmissionController@adminDelete');

    /*-----Comments Admin-----*/
    Route::get('/admin/comments', 'SubCommentController@adminIndex');
    Route::get('/admin/comments/{comment}/status', 'SubCommentController@adminStatus');
    Route::get('admin/comments/{comment}/delete', 'SubCommentController@adminDelete');

    /*-----Users Admin-----*/
    Route::get('/admin/users', 'adminUsersController@Index');
    Route::get('/admin/users/new', 'adminUsersController@userNewIndex');
    Route::post('admin/users/new', 'adminUsersController@store');
    Route::get('admin/profile/{user}/edit', 'adminUsersController@edit');
    Route::post('admin/user/resetPassword', 'Auth\PasswordController@sendResetLinkEmail');
    Route::patch('admin/profile/{user}', 'adminUsersController@update');
    Route::get('/admin/users/{user}/status', 'adminUsersController@status');

    /*-----Roles Admin-----*/
    Route::get('/admin/roles', 'RolesController@Index');
    Route::post('/admin/roles/new', 'RolesController@store');
    Route::get('/admin/roles/{role}/edit', 'RolesController@edit');
    Route::patch('admin/roles/{role}', 'RolesController@update');
    Route::get('admin/roles/{role}/delete', 'RolesController@delete');
    
});





