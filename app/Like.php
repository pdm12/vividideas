<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    //
    /**
     * This is For Submission LIKES
     */

    protected $fillable =[
        'submission_id' ,
        'user_id',
        'like',
    ];



    public function user()
    {
        $this->belongsTo(User::class);
    }

    public function submission()
    {
        $this->belongsTo(Submission::class);
    }

    public static function likeTableCheck($user_id,$sub_id)
    {

            if($like = static::where('user_id',$user_id)->where('submission_id',$sub_id)->get()->first()) {
                return $like->like;
            }
            else{
                return 3;
            }

    }


}
