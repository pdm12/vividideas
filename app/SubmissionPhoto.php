<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Image;


class SubmissionPhoto extends Model
{
    //
    protected $table = 'submission_photos'; //Get the table name

    protected $fillable = ['path','name','thumbnail_path'];

    protected $baseDir = 'submissionPhotos/photos';

    public function submission()
    {
        $this->belongsTo(Submission::class);
    }


    /**
     * name the photo
     * move it to a directory
     *
     * @param $name
     * @return static
     */
    public static function named($name)
    {

        $photo = new static; //Static instance

        return $photo->saveAs($name); //Gets the file name

        //Its done this way so that there will be no same files overriding eachother
        // $name = Carbon::now().rand(1,100).$file->getClientOriginalName();
        //$name = str_replace(' ','-', $name);

        // $photo->path = '/'.$photo->baseDir . '/' .$name;

        //$file->move($photo->baseDir,$name); //

        //  return $photo;


    }

    public function saveAs($name)
    {
        $this->name= sprintf("%s-%s",Carbon::now().rand(1,100),$name);
        $this->name = str_replace(' ','-', $this->name);
        $this->path = sprintf("%s/%s",$this->baseDir,$this->name);
        $this->thumbnail_path = sprintf("%s/tn-%s",$this->baseDir,$this->name);

        return $this;
    }

    public function move(UploadedFile $file)
    {
        $file->move($this->baseDir,$this->name); //

        $this->makeThumbnail();

        return $this;
    }

    public function makeThumbnail()
    {
        Image::make($this->path)
            ->fit(200)
            ->save($this->thumbnail_path);
    }

    public function delete()
    {

        \File::delete([ //To delete from the folder
            $this->path,
            $this->thumbnail_path
        ]);

        parent::delete();
    }




}
