<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBadgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('badges', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id');
            $table->integer('submission_id');

            //If Submissions is deleted the likes associated with that also gets deleted
            // $table->foreign('submission_id')->references('id')->on('submissions')->onDelete('cascade');

            // 1-Great Idea 2-WellDone 3-Excellent 4-Amazing

            $table->integer('badge');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('badges');
    }
}
