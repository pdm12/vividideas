<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_comments', function (Blueprint $table) {
            $table->increments('id');

            $table->text('comment');

            $table->text('submission_id');

            //If Submissions is deleted the comments associated with that also gets deleted
          //  $table->foreign('submission_id')->references('id')->on('submissions')->onDelete('cascade');

            $table->text('user_id');

            $table->integer('comment_status')->default(1)->unsigned()->index();

            $table->integer('flag')->default(0)->unsigned()->index();

            $table->text('video_path');

            $table->integer('vote_count')->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_comments');
    }


}
