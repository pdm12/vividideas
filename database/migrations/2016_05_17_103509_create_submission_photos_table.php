<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissionPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submission_photos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('submission_id')->unsigned();

            //If Task is deleted the photos associated with that also gets deleted
            //$table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
            $table->string('name');

            $table->string('path');

            $table->string('thumbnail_path');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('submission_photos');
    }
}
