@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">

@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Categories</div>
                <div class="panel-body">
                    @foreach($categories as $category)
                        <li><a href="{{ url($category->pathView()) }}">{{ $category->name }}</a></li>
                    @endforeach
                </div>
            </div>

        </div>

        {{--THE TABLE--}}
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Posts</div>
                <div class="panel-body">


                    <table id="myTable" class="table">
                        <thead>
                        <tr>
                            <th class="tableTaskTopic">Topic</th>
                            <th>Submission</th>

                            {{-- <th>Category</th> --}}

                            <th></th>


                        </tr>
                        </thead>


                        <tbody>


                        @foreach($subAll as $sub)

                            <tr>

                                <td class="tableTaskTopic"><a href="{{$sub->path()}}">{{$sub->submission_topic}}<br>

                                        <span class="help-block">Created by: {{$sub->owner->name}}</span>
                                    </a></td>
                                <td><p class="tableTaskDetails"> {{$sub->submission}}<p></td>



                                <td>
                                    <a href="/submissions/{{$sub->id}}" class="btn btn-sm btn-success">Comment
                                    </a>
                                </td>



                            </tr>

                        @endforeach

                        </tbody>


                    </table>








                </div>
            </div>
        </div>
    </div>
    {{--THe Table End--}}
</div>
@endsection


@section('scripts.footer')
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready(function () {
            $('#myTable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "ZeroRecords": "No matching records found",
                "searchDelay": null,
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "columns": [
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": false},

                ],
                "columns": [
                    {"sortable": true},
                    {"sortable": false},
                    {"sortable": false},

                ],


            });

        });

    </script>

    <script>


    </script>


@stop