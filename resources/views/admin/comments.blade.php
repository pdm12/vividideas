@extends('layouts.admin')

@section('content')
    <h2>Submissions</h2>
    <div class="box">
        <table id="dataTable" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>User</th>
                <th>Comment</th>
                <th>Submission Topic</th>
                <th>Status</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($comments as $comment)
                <tr>
                    <td>{{ $comment->commentOwner->name }}</td>
                    <td>{{ $comment->comment }}</td>
                    <td>{{ $comment->getSubmission->submission_topic }}</td>
                    <td>
                        @if($comment->comment_status == 1)
                            <i class="fa fa-check" aria-hidden="true"></i>
                        @else
                            <i class="fa fa-times" aria-hidden="true"></i>
                        @endif
                    </td>
                    <td>
                        @if($comment->comment_status == 1)
                            <a class="btn btn-warning btn-sm" role="button" href="/admin/comments/{{ $comment->id }}/status">Disapprove</a>
                        @else
                            <a class="btn btn-success btn-sm" role="button" href="/admin/comments/{{ $comment->id }}/status">Approve</a>
                        @endif
                    </td>
                    <td><a class="btn btn-danger btn-sm" role="button" onclick="confirmDelete('{{ $comment->id }}')">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        function confirmDelete(id) {
            swal({
                        title: "Are you sure you want to delete this comment?",
                        text: "Once deleted you want be able to recover it!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        window.location.href = "comments/" + id + "/delete";
                        swal("Deleted!", "Your comment has been deleted.", "success");
                    });
        }
    </script>
@stop