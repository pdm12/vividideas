@extends('layouts.admin')

@section('content')

    <script>
        function confirmDelete(id) {
            if (confirm("Are you sure you want to delete the record?") == true) {
                window.location.href = "categories/" + id + "/delete";
            } else {}
        }
    </script>

    <h2>Categories</h2>

    <div class="col-md-5">
        <div class="box">
            <h3>Add New Category</h3>
            <br/>
            @if(count($errors))
                @foreach($errors->all() as $error)
                    <div class="alert alert-warning" role="alert">{{ $error }}</div>
                @endforeach
            @endif
            <form method="post" action="/admin/categories/new">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" placeholder="">
                </div>
                <div class="form-group">
                    <label for="catParent">Parent</label>
                    <select class="form-control" name="catParent">
                        <option value="0">None</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <label for="catDescription">Description</label>
                <textarea class="form-control" name="catDescription" rows="6"></textarea>
                <br/>
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>Add New Category</button>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
    <div class="col-md-7">
        <div class="box">
            <h3>Categories</h3>
            <table class="table table-striped">
                <th>Name</th>
                <th>Description</th>
                <th></th>
                <th></th>
                @foreach($categories as $category)
                <tr>
                    <td><a href="{{ $category->pathEdit() }}">{{ $category->name }}</a></td>
                    <td>{{ $category->description }}</td>
                    <td><a class="btn btn-warning btn-sm" role="button" href="{{ $category->pathEdit() }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a></td>
                    <td><a class="btn btn-danger btn-sm" role="button" onclick="confirmDelete({{ $category->id }})"><i class="fa fa-times" aria-hidden="true"></i>Delete</a></td>
                    @foreach($category->subCategory as $firstNestedSub)
                        <tr>
                            <td><a href="{{ $firstNestedSub->pathEdit() }}">- {{ $firstNestedSub->name }}</a></td>
                            <td>{{ $firstNestedSub->description }}</td>
                            <td><a class="btn btn-warning btn-sm" role="button" href="{{ $firstNestedSub->pathEdit() }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a></td>
                            <td><a class="btn btn-danger btn-sm" role="button" onclick="confirmDelete({{ $firstNestedSub->id }})"><i class="fa fa-times" aria-hidden="true"></i>Delete</a></td>
                        </tr>
                    @endforeach
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@stop
