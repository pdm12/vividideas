@extends('layouts.admin')
@section('content')
    <div class="col-md-12">
        <h2 class="text-center">Welcome to VividIdeas Admin Dashboard!</h2>
    </div>
    <div class="col-md-3">
        <div class="box">
        <h3><i class="fa fa-users" aria-hidden="true"></i><a href="admin/users">Users</a></h3>
        <p>Manage all users</p>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box">
        <h3><i class="fa fa-sitemap" aria-hidden="true"></i><a href="admin/categories">Categories</a></h3>
        <p>Manage all categories</p>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box">
        <h3><i class="fa fa-newspaper-o" aria-hidden="true"></i><a href="admin/submissions">Submissions</a></h3>
        <p>Manage all submissions</p>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box">
        <h3><i class="fa fa-comments-o" aria-hidden="true"></i><a href="admin/comments">Comments</a></h3>
        <p>Manage all comments</p>
        </div>
    </div>
@stop