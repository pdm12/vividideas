@extends('layouts.admin')

@section('content')
    <h2>Submissions</h2>
    <div class="col-md-7">
        <div class="box">
            <h3>Edit Submission</h3>
            @if(count($errors))
                @foreach($errors->all() as $error)
                    <div class="alert alert-warning" role="alert">{{ $error }}</div>
                @endforeach
            @endif
            <form method="post" action="/admin/submissions/{{ $submission->id }}">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Topic</label>
                    <input type="text" class="form-control" name="submission_topic" value="{{ $submission->submission_topic }}">
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <select name="category_id" class="form-control">
                        <option value="{{ $submission->category_id }}">{{ $submission->getCategory->name }}</option>
                        @foreach($category as $cat)
                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Submission</label>
                    <textarea class="form-control" name="submission" rows="10">{{ $submission->submission }}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Update Submission</button>
                </div>
            </form>
        </div>
    </div>
@stop