@extends('layouts.admin')

@section('content')

    <h2>Categories</h2>
    <div class="col-md-5">
        <div class="box">
            <h3>Edit Category</h3>
            <br/>

            @if(count($errors))
                @foreach($errors->all() as $error)
                    <div class="alert alert-warning" role="alert">{{ $error }}</div>
                @endforeach
            @endif
            <form method="post" action="/admin/categories/{{ $category->id }}">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" value="{{ $category->name }}">
                </div>
                <div class="form-group">
                    <label for="parent_id">Parent</label>

                    <select class="form-control" name="parent_id" {{ $category->checkChildExist() }}>
                        <option value="{{ $category->parent_id }}">{{ $category->getParent() }}</option>
                        @if($category->parent_id != 0)
                            <option value="0">None</option>
                        @endif
                        @foreach($categories as $categorys)
                            <option value="{{ $categorys->id }}">{{ $categorys->name }}</option>
                        @endforeach
                    </select>
                </div>
                <label for="description">Description</label>
                <textarea class="form-control" name="description" rows="6">{{ $category->description }}</textarea>
                <br/>
                <button type="submit" class="btn btn-primary"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i>Update Category</button>
            </form>
        </div>
    </div>
@stop
