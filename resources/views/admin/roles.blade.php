@extends('layouts.admin')

@section('content')

    <h1>User Roles</h1>

    <div class="col-md-5">
        <div class="box">
            <h3>Add New Role</h3>
            <br/>

            <form method="post" action="/admin/roles/new">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="control-label">Name</label>
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <label>Description</label>
                <textarea class="form-control" name="description" rows="6"></textarea>
                <br/>
                <button type="submit" class="btn btn-primary">Add New Role</button>

            </form>
        </div>
    </div>
    <div class="col-md-7">
        <div class="box">
            <h3>User Roles</h3>
            <table id="dataTable" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Role ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                @foreach($roles as $role)
                <tr>
                    <td>{{ $role->id }}</td>
                    <td><a href="{{ $role->pathEdit() }}">{{ $role->name }}</a></td>
                    <td>{{ $role->description }}</td>
                    <td><a class="btn btn-warning btn-sm" role="button" href="{{ $role->pathEdit() }}">Edit</a></td>
                    <td><a class="btn btn-danger btn-sm" role="button" onclick="confirmDelete({{ $role->id }})">Delete</a></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <script>
        function confirmDelete(id) {
            swal({
                        title: "Are you sure you want to delete the user role?",
                        text: "Once deleted you wont be able to recover it!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        window.location.href = "roles/" + id + "/delete";
                    });
        }
    </script>
@stop
