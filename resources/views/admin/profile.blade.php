
@extends('layouts.admin')

@section('content')

    <h1>Profile</h1>
    <br/>
<div class="col-md-6">
    <div class="box">
        <form method="post" action="/admin/profile/{{ $user->id }}">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <h3>Name</h3>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label class="control-label">First Name*</label>
                            <input type="text" name="first_name" class="form-control" value="{{ $user->first_name }}">
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label class="control-label">Last Name*</label>
                            <input type="text" name="last_name" class="form-control" value="{{ $user->last_name }}">
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <hr/>
            <h3>Contact Information</h3>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="control-label">Email address</label>
                            <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <hr/>
            <h3>About Yourself</h3>
            <div class="form-group">
                <label>Biographical Info</label>
                <textarea class="form-control" rows="6" name="bio">{{ $user->bio }}</textarea>
                <span class="help-block">Share a little biographical information to fill out your profile. This may be shown publicly.</span>
            </div>
            <br/>
            <button type="submit" class="btn btn-primary">Update Profile</button>
        </form>
    </div>
</div>
    <div class="col-md-6">
        <div class="box">
            <form class="form-horizontal" role="form" method="post" action="/admin/user/resetPassword">
                {{ csrf_field() }}
                <h3>Account Management</h3>
                <br/>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="form-group">
                        <label class="col-md-4 control-label">Email address</label>
                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ $user->email }}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">New Password</label>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-warning btn-sm">Reset Password</button>
                        <span class="help-block">Password reset link will be sent to the specified email address.</span>
                    </div>

                </div>
            </form>
        </div>
    </div>
@stop
