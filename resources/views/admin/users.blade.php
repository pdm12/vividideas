@extends('layouts.admin')

@section('content')

    <h1>All Users</h1>

    <br/>

    <div class="col-md-12">
        <div class="box">
            <h3>Users</h3>
            <table id="dataTable" class="display" cellspacing="0" width="100%">
                <thead>
                    <th>Role</th>
                    <th>Username</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->role->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->first_name }}</td>
                            <td>{{ $user->last_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @if($user->status == 1)
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @else
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td>
                                @if($user->status == 1)
                                    <a class="btn btn-warning btn-sm" role="button" href="/admin/users/{{ $user->id }}/status">Disapprove</a>
                                @else
                                    <a class="btn btn-success btn-sm" role="button" href="/admin/users/{{ $user->id }}/status">Approve</a>
                                @endif
                            </td>
                            <td><a class="btn btn-warning btn-sm" role="button" href="{{ $user->pathEdit() }}">Edit</a></td>
                            <td><a class="btn btn-danger btn-sm" role="button" onclick="confirmDelete({{ $user->id }})">Delete</a></td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
        </div>
    </div>
@stop
