@extends('layouts.admin')

@section('content')



    <h2>Submissions</h2>
    <div class="box">
        <table id="dataTable" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Submission Topic</th>
                    <th>Submission</th>
                    <th>Category</th>
                    <th>User</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($submissions as $submission)
                <tr>
                    <td><a href="{{ $submission->pathEdit() }}">{{ $submission->submission_topic }}</a></td>
                    <td>{{ $submission->submission }}</td>
                    <td>{{ $submission->getCategory->name }}</td>
                    <td>{{ $submission->owner->name }}</td>
                    <td><a class="btn btn-warning btn-sm" role="button" href="{{ $submission->pathEdit() }}">Edit</a></td>
                    <td><a class="btn btn-danger btn-sm" role="button" onclick="confirmDelete('{{ $submission->id }}', '{{ $submission->submission_topic }}')">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        function confirmDelete(id, topic) {
            swal({
                title: "Are you sure you want to delete this submission?",
                text: "Once deleted you want be able to recover it!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                window.location.href = "submissions/" + id + "/delete";
                swal("Deleted!", "Your submission: " + topic + "has been deleted.", "success");
            });
        }
    </script>
@stop