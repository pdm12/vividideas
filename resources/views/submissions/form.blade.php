{{csrf_field()}} {{--This token is used to verify that the authenticated user is the one actually making the requests to the application.--}}


<div class="form-group">
    <label for="category_id" class=""> Category : </label>

    <select name="category_id" class="form-control">

        <option value="">Category</option>


        @foreach($category as $cat)
            <option value="{{$cat->id}}">{{$cat->name}}</option>

        @endforeach


    </select>

</div>


<hr>

<div class="form-group">
    <label for="submission_topic">Topic</label>
    <input type="text" class="form-control" id="topic" name="submission_topic"
           placeholder="Enter the topic for the idea" value="{{old('topic')}}" required>
    <input type="hidden" class="form-control" id="user_id" name="user_id" placeholder=""
           value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

</div>


<div class="form-group">
    <i class="fa fa-comment-o" aria-hidden="true"></i>
    <label for="submission">Describe the Idea </label>
    <textarea class="form-control" rows="10" name="submission" value="{{old('submission')}}" required></textarea>
</div>

<div class="form-group">

    <input class="form-control" type="text" name="video_path" id="video_path" placeholder="Enter the video path" class="form-control"
           value="">
</div>

<hr>






