{{--FOR EDIT--}}
@foreach($sub->photos->chunk(3) as $set)
    <div class="row">
        @foreach($set as $photo)

            <div class="col-md-4 gallery_image">

                @if($user && $user->owns($sub))

                    <form method="post" action="/submissionphotos/{{$photo->id}}">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class=" btn btn-sm btn-danger"><i class="fa fa-trash-o"
                                                                                aria-hidden="true"></i>
                        </button>

                    </form>
                @endif


                <a href="/{{$photo->path}}" data-lity>

                    <img src="/{{$photo->thumbnail_path}}" alt="">

                </a>
            </div>

        @endforeach

    </div>
@endforeach




@if($user && $user->owns($sub))
    {{--If We have a user and the user owns the submissions then show--}}

    <hr>
    <form id="addSubmissionPhotoForm"
          action="{{route('store_submission_photo',[$sub->id])}}"
          method="post"
          class="dropzone">


        {{csrf_field()}}
    </form>

@endif