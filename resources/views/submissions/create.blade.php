@extends('layouts.app')

@section('css.header')
    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/lity.min.css">
@stop

@section('content')


    <div class="container">

        <div class="row">



        <form method="post" id="subForm" action="/submissions" enctype="multipart/form-data" >

            <div class="col-md-8">
            @include('submissions.form')
            </div>{{-- End of Coulmn--}}

        </form>

            <div class="col-md-4">
                {{--Do FOR FINALS--}}
                {{--@include('submissions.createAddImage')--}}
            </div>{{-- End of Coulmn--}}

        </div>{{--End of Row--}}


        {{--Upload Video--}}
        <div class="form-group">
            <button class="btn btn-success " id="btnVideoShow">
                Upload Video
            </button>


            <button type="submit" id="btnaddSub" class="btn btn-lg btn-primary ">
                <i class="fa fa-plus fa-spin fa-1x fa-fw margin-bottom" aria-hidden="true"></i>
                Submit Idea
            </button>
        </div>

    </div>

@endsection


@section('scripts.footer')

    <script src="/js/dropzone.js"></script>
    <script src="/js/lity.js"></script>

    <script>

        Dropzone.options.addSubmissionPhotoForm = {

            dictDefaultMessage: 'Drag and drop any images that might be helpful in explaining the submission',

            paramName: 'photo',

            maxFilesize: 3,

            acceptedFiles: '.jpg, .jpeg, .png, .bmp'


        };


    </script>

    <script>

        $('button#btnaddSub').on('click', function () {
                    $("#subForm").submit();
                });


        $("#video_path").hide();

        $("#btnVideoShow").click(function(){
            $("#video_path").show();
        });



    </script>



@stop