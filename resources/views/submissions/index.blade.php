@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">

@stop

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="box">
                    <h3>{{$user->first_name}} {{$user->last_name}}</h3>
                    <p>{{$user->bio}}</p>
                    <hr/>
                    <h4>Discussion Activity</h4>
                    <table class="table">
                        <tr>
                            <td>Submissions:</td>
                            <td>{{$subPoints}}</td>
                        </tr>
                        <tr>
                            <td>Comments:</td>
                            <td>{{$commentPoints}}</td>
                        </tr>
                        <tr>
                            <td>Upvotes:</td>
                            <td>{{$totalUpVotes}}</td>
                        </tr>
                        <tr>
                            <td>Downvotes:</td>
                            <td>{{$totalDownVotes}}</td>
                        </tr>
                        <hr/>

                        <tr>
                            <td><h4>Total Points Earned:</h4></td>
                            <td><h4>{{$subPoints+$commentPoints+$totalUpVotes-$totalDownVotes}}</h4></td>
                        </tr>

                    </table>
                    <hr/>
                    <h4>Badges</h4>

                    <table class="table">
                        <tr>

                            <td><p><button type="button" class="btn btn-sm btn-info">Great Idea!</button></p>

                            </td>
                            <td>{{$greatVotes}}</td>
                        </tr>
                        <tr>

                            <td><p><button type="button" class="btn btn-sm btn-warning">Well Done!</button></p>

                            </td>
                            <td>{{$welldoneVotes}}</td>
                        </tr>
                        <tr>

                            <td><p><button type="button" class="btn btn-sm btn-danger">Excellent!</button></p>

                            </td>
                            <td>{{$excellentVotes}}</td>
                        </tr>
                        <tr>

                            <td><p><button type="button" class="btn btn-sm btn-default">Awesome!</button> </p>

                            </td>
                            <td>{{$awesomeVotes}}</td>
                        </tr>
                        <hr/>

                        <tr>
                            <td><h4>Total Badges Achieved:</h4></td>
                            <td><h4>{{$greatVotes+$welldoneVotes+$excellentVotes+$awesomeVotes}}</h4></td>
                        </tr>

                    </table>



                </div>
            </div>
            <div class="col-md-9">
                <h2>Submitted Ideas </h2>
                <hr>
            <table id="myTable" class="table">
                <thead>
                <tr>
                    <th>Submission Id</th>

                    <th >Topic</th>
                    <th>Submission </th>

                    {{-- <th>Category</th> --}}

                    <th></th>
                    <th></th>


                </tr>
                </thead>


                <tbody>



                @foreach($submissions as $submission)
                    <tr>
                        <td><a href="{{$submission->path()}}">{{$submission->id}} </a></td>

                        <td><a href="{{$submission->path()}}">{{$submission->submission_topic}} </a></td>

                        <td><p > {{$submission->submission}}<p></td>

                        {{--<td>{{$task->category_id}}</td>--}}


                        <td>
                            <a href="/submissions/{{$submission->id}}/edit" class="btn btn-sm btn-warning">Edit
                            </a>
                        </td>



                        <td>

                            <a class="btn btn-sm btn-danger" href="/submissions/{{$submission->id}}" data-method="delete"
                               data-token="{{csrf_token()}}" data-confirm="Are you sure?">
                                <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                            </a>


                        </td>


                    </tr>

                @endforeach

                </tbody>


            </table>

            </div>

        </div>
    </div>
    </div>
@endsection

@section('scripts.footer')
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready(function () {
            $('#myTable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "ZeroRecords": "No matching records found",
                "searchDelay": null,
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "columns": [
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": false},
                    {"searchable": false},

                ],
                "columns": [
                    {"sortable": true},
                    {"sortable": true},
                    {"sortable": false},
                    {"sortable": false},
                    {"sortable": false},

                ],


            });

        });

    </script>

    <script>

(function() {

    var laravel = {
        initialize: function() {
            this.methodLinks = $('a[data-method]');
            this.token = $('a[data-token]');
            this.registerEvents();
        },

        registerEvents: function() {
            this.methodLinks.on('click', this.handleMethod);
        },

        handleMethod: function(e) {
            var link = $(this);
            var httpMethod = link.data('method').toUpperCase();
            var form;

            // If the data-method attribute is not PUT or DELETE,
            // then we don't know what to do. Just ignore.
            if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
                return;
            }

            // Allow user to optionally provide data-confirm="Are you sure?"
            if ( link.data('confirm') ) {
                if ( ! laravel.verifyConfirm(link) ) {
                    return false;
                }
            }

            form = laravel.createForm(link);
            form.submit();

            e.preventDefault();
        },

        verifyConfirm: function(link) {
            return confirm(link.data('confirm'));
        },


        createForm: function(link) {
            var form =
                    $('<form>', {
                        'method': 'POST',
                        'action': link.attr('href')
                    });

            var token =
                    $('<input>', {
                        'type': 'hidden',
                        'name': '_token',
                        'value': link.data('token')
                    });

            var hiddenInput =
                    $('<input>', {
                        'name': '_method',
                        'type': 'hidden',
                        'value': link.data('method')
                    });

            return form.append(token, hiddenInput)
                    .appendTo('body');
        }
    };

    laravel.initialize();

})();


    </script>


@stop