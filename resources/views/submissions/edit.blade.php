@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/lity.min.css">


@stop

@section('content')

    @include('includes.message_block')

    @if($user && $user->owns($sub))

    <div class="container">

        <h1>Edit the Submission</h1>

        <div class="row">

            <div class="col-md-6">

                <form method="post" action="/submissions/{{$sub->id}}" enctype="multipart/form-data">

                    @include('submissions.editform')

                </form>
            </div>{{-- End of Coulmn--}}


            <div class="col-md-6 gallery">

                @include('submissions.ShowAddImage')
            </div>


        </div>{{--End of Row--}}

    </div>

    @else

        {{flash()->error('You are not allowed to view this page','Only Submission owner can delete a Submission')}}
        <script type="text/javascript">
            window.location = "{{ url('/home') }}";//here double curly bracket
        </script>
    @endif

@endsection



@section('scripts.footer')

    <script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

    <script src="/js/dropzone.js"></script>
    <script src="/js/lity.js"></script>

    <script>

        Dropzone.options.addSubmissionPhotoForm = {

            dictDefaultMessage: 'Drag and drop any images that might be helpful in explaining the task',

            paramName: 'photo',

            maxFilesize: 3,

            acceptedFiles: '.jpg, .jpeg, .png, .bmp'


        };


    </script>

@stop