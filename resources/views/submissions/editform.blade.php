{{csrf_field()}} {{--This token is used to verify that the authenticated user is the one actually making the requests to the application.--}}
<input type="hidden" name="_method" value="PUT">



        <div class="form-group">
            <label for="category_id" class=""> Category : </label>

            <select name="category_id" class="form-control">

                <option value="{{$sub->category_id}}">{{$sub->getCategory->name}}</option>

                @foreach($category as $cat)
                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                    {{--   <option>{{$cat->subCategories[0]->name}}</option> --}}

                @endforeach


            </select>

        </div>


        <hr>

        <div class="form-group">
            <label for="submission_topic" >Topic</label>
            <input type="text" class="form-control" id="submission_topic" name="submission_topic" placeholder="Enter the topic for the Submission" value="{{$sub->submission_topic}}" required>


            <input type="hidden" class="form-control" id="user_id" name="user_id" placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

        </div>



        <div class="form-group">
            <i class="fa fa-comment-o" aria-hidden="true"></i>
            <label for="submission">Describe the Idea</label>
            <textarea class="form-control"  rows="10" name="submission" value="" required>{{$sub->submission}}</textarea>
        </div>


        {{--<div class="form-group">--}}
        {{--<i class="fa fa-picture-o" aria-hidden="true"></i>--}}

        {{--<label for="photo">Add pictures to support the task request</label>--}}
        {{--<input type="file" class="form-control" id="photo"  name="photo" >--}}
        {{--</div>--}}


        <hr>

        @if($sub->video_path)
            <div class="form-group">
                <label for="submission">Enter the Video Path</label>
                <input class="form-control" type="text" name="video_path" id="video_path" placeholder="" class="form-control"
                       value="{{$sub->video_path}}">
            </div>
            @endif


        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-primary">
                <i class="fa fa-plus fa-spin fa-1x fa-fw margin-bottom" aria-hidden="true"></i>

                Submit Idea</button>
        </div>


    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <! Will be used to display an alert to the user>
        </div>
    </div>



