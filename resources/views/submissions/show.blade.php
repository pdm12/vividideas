@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/lity.min.css">


@stop



@section('content')

    <div class="container">
        <div class="row">

            {{--Categories Start--}}
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Categories</div>
                    <div class="panel-body cat-list">
                        <ul>
                            @foreach($categories as $category)
                                <li><a href="{{ url($category->pathView()) }}">{{ $category->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>

            {{--Catgories End--}}


            <div class="col-md-9 ">{{--Outer Column--}}


                <h2>{{$sub->submission_topic}}</h2>


                <h4><span class="help-block">Created by {{$sub->owner->name}}</span></h4>

                {{--Interaction--}}
                {{--<h4><span class="help-block">Upvotes {{$subTotalLikes}}</span></h4>--}}

                @if($user && !$user->owns($sub))


                    @if($ifLike == 1)
                        <div class="interaction">
                            <a href="#" id=""><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
                            <a href="#" id="subLike">Upvoted</a> |
                            <a href="#" id="subDislike">Downvote</a> |
                        </div>

                    @elseif($ifLike == 0)
                        <div class="interaction">
                            <a href="#" id="subLike">Upvote</a> |
                            <a href="#" id="subDislike">Downvoted</a>
                            <a href="#" id=""><i class="fa fa-thumbs-down" aria-hidden="true"></i>
                            </a>

                        </div>

                    @else($ifLike == 3)
                        <div class="interaction">
                            <a href="#" id="subLike">Upvote</a> |
                            <a href="#" id="subDislike">Downvote</a> |
                        </div>

                    @endif

                @endif

                <form id="subLikeForm"
                      action="{{route('store_like_sub',[$sub->id])}}"
                      method="post"
                >
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" id="like" name="like"
                           placeholder="" value="1">{{--To pass in the user's Id--}}
                    <input type="hidden" class="form-control" id="user_id" name="user_id"
                           placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                    <input type="hidden" class="form-control" id="submission_id" name="submission_id"
                           placeholder="" value="{{$sub->id}}">{{--To pass in the submission's Id--}}
                </form>

                <form id="subDislikeForm"
                      action="{{route('store_like_sub',[$sub->id])}}"
                      method="post"
                >
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" id="like" name="like"
                           placeholder="" value="0">{{--To pass in the user's Id--}}
                    <input type="hidden" class="form-control" id="user_id" name="user_id"
                           placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                    <input type="hidden" class="form-control" id="submission_id" name="submission_id"
                           placeholder="" value="{{$sub->id}}">{{--To pass in the submission's Id--}}
                </form>

                {{--End Interaction--}}
















                {{--End of Topic and Submitted BY--}}
                <hr>
                <div class="description">
                    {!!  nl2br($sub->submission) !!}  {{-- nl2br:To preserve the spaces --}}
                </div>

                {{--Gallery--}}
                <div class="row">
                    <div class="col-md-12 gallery">
                        @foreach($sub->photos->chunk(3) as $set)
                            <div class="row">
                                @foreach($set as $photo)

                                    <div class="col-md-4 gallery_image">
                                        @if($user && $user->owns($sub))
                                            <form method="post" action="/submissionphotos/{{$photo->id}}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class=" btn btn-sm btn-danger"><i
                                                            class="fa fa-trash-o"
                                                            aria-hidden="true"></i>
                                                </button>

                                            </form>
                                        @endif


                                        <a href="/{{$photo->path}}" data-lity>

                                            <img src="/{{$photo->thumbnail_path}}" alt="">

                                        </a>
                                    </div>

                                @endforeach


                            </div>
                        @endforeach
                        @if($sub->video_path)
                            <iframe width="420" height="315" src="{{$sub->video_path}}"
                                    frameborder="0" allowfullscreen></iframe>
                        @endif

                        {{--@foreach($task->photos as $photo)--}}

                        {{--<img src="/{{$photo->thumbnail_path}}" alt="">--}}

                        {{--@endforeach--}}


                        @if($user && $user->owns($sub))
                            {{--If We have a user and the user owns the tasks then show--}}

                            <hr>
                            <form id="addSubmissionPhotoForm"
                                  action="{{route('store_submission_photo',[$sub->id])}}"
                                  method="post"
                                  class="dropzone">


                                {{csrf_field()}}
                            </form>

                        @endif

                    </div> {{--End Of Col Gallery--}}

                </div>
                {{--Gallery Row End--}}





                @if($user && !$user->owns($sub))






                <br>
{{--Badge--}}
                <div class="dropdown">
                    <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown">Give A Badge
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="#" id="badgeGreat"  class="btn btn-primary">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                                Great Idea<span class="badge"></span></a></li>
                        <li><a href="#" id="badgeWelldone" class="btn btn-success">
                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                Well Done<span class="badge"></span></a></li>
                        <li><a  href="#" id="badgeExcellent" class="btn btn-info">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                Excellent<span class="badge"></span></a></li>
                        <li><a  href="#" id="badgeAmazing" class="btn btn-warning">
                                <i class="fa fa-trophy" aria-hidden="true"></i>Amazing
                                <span class="badge"></span>
                            </a></li>

                    </ul>
                </div>



                {{--Badge Start--}}
                <form id="badgeGreatForm"
                      action="{{route('store_badge_sub',[$sub->id])}}"
                      method="post"
                >
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" id="badge" name="badge"
                           placeholder="" value="1">{{--To pass in the user's Id--}}
                    <input type="hidden" class="form-control" id="user_id" name="user_id"
                           placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                    <input type="hidden" class="form-control" id="submission_id" name="submission_id"
                           placeholder="" value="{{$sub->id}}">{{--To pass in the submission's Id--}}
                </form>

                <form id="badgeWelldoneForm"
                      action="{{route('store_badge_sub',[$sub->id])}}"
                      method="post"
                >
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" id="badge" name="badge"
                           placeholder="" value="2">{{--To pass in the user's Id--}}
                    <input type="hidden" class="form-control" id="user_id" name="user_id"
                           placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                    <input type="hidden" class="form-control" id="submission_id" name="submission_id"
                           placeholder="" value="{{$sub->id}}">{{--To pass in the submission's Id--}}
                </form>

                <form id="badgeExcellentForm"
                      action="{{route('store_badge_sub',[$sub->id])}}"
                      method="post"
                >
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" id="badge" name="badge"
                           placeholder="" value="3">{{--To pass in the user's Id--}}
                    <input type="hidden" class="form-control" id="user_id" name="user_id"
                           placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                    <input type="hidden" class="form-control" id="submission_id" name="submission_id"
                           placeholder="" value="{{$sub->id}}">{{--To pass in the submission's Id--}}
                </form>

                <form id="badgeAmazingForm"
                      action="{{route('store_badge_sub',[$sub->id])}}"
                      method="post"
                >
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" id="badge" name="badge"
                           placeholder="" value="4">{{--To pass in the user's Id--}}
                    <input type="hidden" class="form-control" id="user_id" name="user_id"
                           placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                    <input type="hidden" class="form-control" id="submission_id" name="submission_id"
                           placeholder="" value="{{$sub->id}}">{{--To pass in the submission's Id--}}
                </form>
                {{--Badge End--}}

                @endif

















                {{--Comment Form--}}
                <div class="row">
                    <div class="col-md-12">
                        <hr/>
                        <h3>Comments</h3>

                        <form id="commentAddform" action="{{route('comment.create',[$sub->id])}}" method="post">
                            {{csrf_field()}}

                            <div class="form-group">
                                <textarea class="form-control tarea" name="comment" id="comment" rows="6"
                                          placeholder="Give a Comment"></textarea>

                                <input type="hidden" class="form-control" id="user_id" name="user_id"
                                       placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                                <input type="hidden" class="form-control" id="submission_id" name="submission_id"
                                       placeholder="" value="{{$sub->id}}">
                                {{--To pass in the submission's Id--}}
                            </div>

                            <div class="form-group">
                                <input class="form-control" type="text" name="video_path" id="video_path"
                                       class="form-control" placeholder="Enter the url path for the video" value="">
                            </div>


                            <div class="form-group">
                            </div>

                        </form>

                        <button id="btnaddComment" type="submit" class="btn btn-primary">Comment</button>


                        <button class="btn btn-success " id="btnVideoShow">
                            Upload Video
                        </button>




                        {{--<hr>--}}
                        {{--<form id="addCommentPhotoForm"--}}
                        {{--{{route('store_comment_photo',[$sub->id])}}--}}
                        {{--action="/foobar"--}}
                        {{--method="post"--}}
                        {{--class="dropzone">--}}
                        {{--{{csrf_field()}}--}}
                        {{--</form>--}}
                        {{--<hr>--}}


                    </div>

                </div>

                {{--Comment Form End--}}



                {{--Show Comments--}}
                <div class="row">
                    <hr/>
                    <div class="col-md-12">
                        <div class="box">


                            @foreach($sub->comments as $comment)

                                @if($comment->comment_status == 1)

                                    <span class="help-block">{{$comment->commentOwner->name}}</span>
                                    <p>
                                        {{$comment->comment}}

                                    </p>

                                    @if($comment->video_path)
                                        <iframe width="420" height="315" src="{{$comment->video_path}}"
                                                frameborder="0" allowfullscreen></iframe>
                                    @endif


                                    {{--<span class="help-block">Upvote</span>--}}

                                    {{--Comment Interaction--}}


                                    {{--Interaction--}}
                                    {{--<h4><span class="help-block">Upvotes {{$subTotalLikes}}</span></h4>--}}

                                    @if($user && !$user->owns($comment))


                                            <div class="interaction">
                                                <a href="#" id=""><i class="fa fa-thumbs-o-up"
                                                                     aria-hidden="true"></i></a>
                                                <a href="#" id="commentLike">Upvote</a> |
                                                <a href="#" id="commentDislike">Downvote</a> |
                                                <span class="help-block">Votes: {{$comment->vote_count}}</span>
                                            </div>

                                    @endif
                                    @if($user && $user->owns($comment))
                                        <div class="interaction">
                                            <a href="#" id="commentEdit">Edit</a> |
                                            <a href="/submissions/{{$sub->id}}/comment/{{$comment->id}}"
                                               data-method="delete" data-token="{{csrf_token()}}"
                                               data-confirm="Are you sure?">Delete</a> |
                                            <span class="help-block">Votes: {{$comment->vote_count}}</span>

                                        </div>










                                        <!--Modal start -->
                                        <div class="modal fade" tabindex="-1" role="dialog" id="edit-modal">


                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                                    aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Edit Comment</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form id="commentEditForm" action="{{route('comment.edit',[$sub->id,$comment->id])}}"
                                                              method="post">
                                                            {{csrf_field()}} {{--This token is used to verify that the
                                                                        authenticated user is the one actually making
                                                                         the requests to the application.--}}
                                                            <input type="hidden" name="_method" value="PUT">

                                                            <div class="form-group">
                                                                <label for="post-body">Edit the Comment</label>
                                <textarea class="form-control" name="comment" id="comment"
                                          placeholder="Give a Comment" rows="5"></textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" name="video_path" id="video_path"
                                                                       class="form-control" placeholder="Enter the url path for the video" value="">
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary" id="modal-save">Save changes</button>
                                                            </div>

                                                        </form>

                                                    </div>



                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                        {{-- Modal End--}}















                                    @endif



                                    <form id="commentLikeForm"
                                          action="{{route('store_like_comment',[$comment->id])}}"
                                          method="post"
                                    >
                                        {{csrf_field()}}
                                        <input type="hidden" class="form-control" id="like" name="like"
                                               placeholder="" value="1">{{--To pass in the user's Id--}}
                                        <input type="hidden" class="form-control" id="user_id" name="user_id"
                                               placeholder=""
                                               value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                                        <input type="hidden" class="form-control" id="sub_comment_id"
                                               name="sub_comment_id"
                                               placeholder=""
                                               value="{{$comment->id}}">{{--To pass in the submission's Id--}}
                                    </form>

                                    <form id="commentDislikeForm"
                                          action="{{route('store_like_comment',[$comment->id])}}"
                                          method="post"
                                    >
                                        {{csrf_field()}}
                                        <input type="hidden" class="form-control" id="like" name="like"
                                               placeholder="" value="0">{{--To pass in the user's Id--}}
                                        <input type="hidden" class="form-control" id="user_id" name="user_id"
                                               placeholder=""
                                               value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                                        <input type="hidden" class="form-control" id="sub_comment_id"
                                               name="sub_comment_id"
                                               placeholder=""
                                               value="{{$comment->id}}">{{--To pass in the submission's Id--}}
                                    </form>

                                    {{--End Interaction--}}


                                    {{--Comment Interaction--}}
                                @endif
                                    <hr/>

                            @endforeach


                        </div>
                    </div>
                </div>
                {{--Show Comments--}}


            </div> {{--End Of Col--}}


        </div>{{--End Of Row--}}


        <br>



        {{-- Modal --}}


    </div>




@stop



@section('scripts.footer')

    <script src="/js/dropzone.js"></script>
    <script src="/js/lity.js"></script>

    {{--This js files contains the imp functions--}}
    <script src="/js/thisApp.js"></script>

    <script>

        Dropzone.options.addSubmissionPhotoForm = {

            dictDefaultMessage: 'Drag and drop any images that might be helpful in explaining the submssion',

            paramName: 'photo',

            maxFilesize: 3,

            acceptedFiles: '.jpg, .jpeg, .png, .bmp'


        };
    </script>

    <script>

        Dropzone.options.addCommentPhotoForm = {

            dictDefaultMessage: 'Drag and drop any images that might be helpful in explaining the Comment',

            paramName: 'photo',

            maxFilesize: 3,

            acceptedFiles: '.jpg, .jpeg, .png, .bmp'


        };
    </script>

    <script>
        {{--var token = '{{ Session::token() }}';--}}
       {{--var urlLike = '{{ route('like') }}';--}}

        $('#subLike').on('click', function () {
            $("#subLikeForm").submit();
        });
        $('#subDislike').on('click', function () {
            $("#subDislikeForm").submit();
        });


        $('#badgeGreat').on('click', function () {
            $("#badgeGreatForm").submit();
        });
        $('#badgeWelldone').on('click', function () {
            $("#badgeWelldoneForm").submit();
        });
        $('#badgeExcellent').on('click', function () {
            $("#badgeExcellentForm").submit();
        });
        $('#badgeAmazing').on('click', function () {
            $("#badgeAmazingForm").submit();
        });


        $('#commentLike').on('click', function () {
            $("#commentLikeForm").submit();

        });
        $('#commentDislike').on('click', function () {
            $("#commentDislikeForm").submit();
        });



    </script>


        <script>
        (function() {

            var laravel = {
                initialize: function() {
                    this.methodLinks = $('a[data-method]');
                    this.token = $('a[data-token]');
                    this.registerEvents();
                },

                registerEvents: function() {
                    this.methodLinks.on('click', this.handleMethod);
                },

                handleMethod: function(e) {
                    var link = $(this);
                    var httpMethod = link.data('method').toUpperCase();
                    var form;

                    // If the data-method attribute is not PUT or DELETE,
                    // then we don't know what to do. Just ignore.
                    if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
                        return;
                    }

                    // Allow user to optionally provide data-confirm="Are you sure?"
                    if ( link.data('confirm') ) {
                        if ( ! laravel.verifyConfirm(link) ) {
                            return false;
                        }
                    }

                    form = laravel.createForm(link);
                    form.submit();

                    e.preventDefault();
                },

                verifyConfirm: function(link) {
                    return confirm(link.data('confirm'));
                },


                createForm: function(link) {
                    var form =
                            $('<form>', {
                                'method': 'POST',
                                'action': link.attr('href')
                            });

                    var token =
                            $('<input>', {
                                'type': 'hidden',
                                'name': '_token',
                                'value': link.data('token')
                            });

                    var hiddenInput =
                            $('<input>', {
                                'name': '_method',
                                'type': 'hidden',
                                'value': link.data('method')
                            });

                    return form.append(token, hiddenInput)
                            .appendTo('body');
                }
            };

            laravel.initialize();

        })();
    </script>


    <script>
        $('#commentEdit').on('click', function () {

            $('#edit-modal').modal();
        });


    </script>

@stop